output "vm_id" {
  value = proxmox_virtual_environment_vm.linux_vm.vm_id
}
output "vm_ip" {
  value = proxmox_virtual_environment_vm.linux_vm.ipv4_addresses[1][0]
}
output "vm_mac" {
  value = proxmox_virtual_environment_vm.linux_vm.network_device[0].mac_address
}

