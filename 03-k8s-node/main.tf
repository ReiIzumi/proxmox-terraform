data "local_file" "ssh_public_key" {
  filename = var.vm.public_key
}

resource "proxmox_virtual_environment_file" "cloud-config" {
  node_name    = var.proxmox_config.node_name
  content_type = "snippets"
  datastore_id = var.proxmox_config.snippet_datastore

  source_raw {
    data = <<EOF
#cloud-config
hostname: ${var.vm.name}
users:
  - default
  - name: ${var.vm.user_name}
    groups:
      - sudo
    shell: /bin/bash
    ssh_authorized_keys:
      - ${trimspace(data.local_file.ssh_public_key.content)}
    sudo: ALL=(ALL) NOPASSWD:ALL

package_update: true
package_upgrade: true
packages:
  - qemu-guest-agent
  - net-tools
  - vim
  - nagios-nrpe-server
  - gnupg2
  - open-iscsi
  - nfs-common

resize_rootfs: true

disk_setup:
  /dev/vdb:
    table_type: 'mbr'
    layout: true
    overwrite: true

fs_setup:
  - label: longhorn
    filesystem: 'ext4'
    device: /dev/vdb
    partition: vdb1
    overwrite: true

mounts:
  - [ /dev/vdb1, /var/lib/longhorn, ext4, "defaults", "0", "2" ]

write_files:
  - path: /etc/ssh/banner
    permissions: '0644'
    owner: root:root
    content: |
      ========== ========== ==========
         Welcome to ${var.vm.name}
      ========== ========== ==========
      You are accessing a private server
      Unauthorized access is not allowed

  - path: /etc/systemd/timesyncd.conf
    permissions: '0644'
    owner: root:root
    content: |
      [Time]
      NTP=${var.vm_ntp.ntp_primary} 
      FallbackNTP=${var.vm_ntp.ntp_secondary}

  - path: /etc/nagios/nrpe_local.cfg
    permissions: '0644'
    owner: root:root
    content: |
      allowed_hosts=127.0.0.1,::1,${var.vm_nagios.nagios_server}      
      command[check_load]=/usr/lib/nagios/plugins/check_load -r -w .80,.75,.70 -c .90,.85,.80
      command[check_disk_root]=/usr/lib/nagios/plugins/check_disk -w 20% -c 10% -p /
      command[check_disk_longhorn]=/usr/lib/nagios/plugins/check_disk -w 20% -c 10% -p /var/lib/longhorn
      command[check_total_procs]=/usr/lib/nagios/plugins/check_procs -w 400 -c 600

  - path: /etc/modules-load.d/k8s.conf
    permissions: '0644'
    owner: root:root
    content: |
      overlay
      br_netfilter

  - path: /etc/sysctl.d/99-kubernetes-cri.conf
    permissions: '0644'
    owner: root:root
    content: |
      net.ipv4.ip_forward = 1
      net.bridge.bridge-nf-call-iptables = 1
      net.bridge.bridge-nf-call-ip6tables = 1

runcmd:
  - echo 'Configuring SSH'
  - sed -i 's@#Banner none@Banner /etc/ssh/banner@' /etc/ssh/sshd_config
  - echo 'Configuring QEMU Guest Agent'
  - systemctl enable qemu-guest-agent
  - systemctl start qemu-guest-agent
  - echo 'Configuring Nagios'
  - systemctl enable nagios-nrpe-server
  - echo 'Creating Longhorn disk'
  - mkfs.ext4 /dev/vdb1
  - mkdir -p /var/lib/longhorn
  - mount /dev/vdb1 /var/lib/longhorn
  - echo 'Preparing Kubernetes dependencies'
  - modprobe overlay
  - modprobe br_netfilter
  - sysctl --system
  - KUBERNETES_VERSION=${var.k8s_version}
  - echo 'Adding CRI-O repository'
  - curl -fsSL https://pkgs.k8s.io/addons:/cri-o:/stable:/$KUBERNETES_VERSION/deb/Release.key | gpg --dearmor -o /etc/apt/keyrings/cri-o-apt-keyring.gpg
  - echo "deb [signed-by=/etc/apt/keyrings/cri-o-apt-keyring.gpg] https://pkgs.k8s.io/addons:/cri-o:/stable:/$KUBERNETES_VERSION/deb/ /" | tee /etc/apt/sources.list.d/cri-o.list
  - echo 'Adding Kubernetes repository'
  - curl -fsSL https://pkgs.k8s.io/core:/stable:/$KUBERNETES_VERSION/deb/Release.key | gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
  - echo "deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/$KUBERNETES_VERSION/deb/ /" | tee /etc/apt/sources.list.d/kubernetes.list
  - apt-get update
  - apt-get install -y cri-o kubelet kubeadm kubectl
  - systemctl enable crio

power_state:
  mode: reboot
  message: "Rebooting the system after cloud-init completion"
  timeout: 30
  condition: True
EOF

    file_name = "k8s_node_${var.vm.name}.yaml"
  }
}

resource "proxmox_virtual_environment_vm" "linux_vm" {
  node_name = var.proxmox_config.node_name

  pool_id     = var.vm.pool_id
  name        = var.vm.name
  description = var.vm.description
  tags        = var.vm.tags

  operating_system {
    type = "l26" # Linux Kernel 2.6 - 5.X.
  }
  keyboard_layout = "es"

  startup {
    order      = var.vm.startup_order
    up_delay   = "60"
    down_delay = "60"
  }

  agent {
    enabled = true
  }

  cpu {
    cores = var.vm_hardware.cpus
    type  = "x86-64-v2-AES" # >= Intel Westmere (2006)
  }

  memory {
    dedicated = var.vm_hardware.memory
  }

  disk {
    datastore_id = var.proxmox_config.vm_datastore
    file_id      = var.proxmox_config.iso
    interface    = "virtio0"
    discard      = "on"
    size         = var.vm_hardware.disk_root_size
  }

  disk {
    datastore_id = var.proxmox_config.vm_datastore
    interface    = "virtio1"
    discard      = "on"
    size         = var.vm_hardware.disk_longhorn_size
    file_format  = "raw"
  }

  network_device {
    mac_address = var.vm_hardware.mac_address
  }

  initialization {
    datastore_id = var.proxmox_config.vm_datastore
    ip_config {
      ipv4 {
        address = "dhcp"
      }
    }

    user_data_file_id = proxmox_virtual_environment_file.cloud-config.id
  }
}
