resource "proxmox_virtual_environment_download_file" "ubuntu_cloud_image" {
  content_type = "iso"
  datastore_id = var.iso_storage.datastore_id
  node_name    = var.iso_storage.node_name
  url          = var.iso_url
}
