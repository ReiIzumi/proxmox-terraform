terraform {
  backend "s3" {
    endpoint                    = var.endpoint
    bucket                      = var.bucket
    region                      = "us-east-1"
    access_key                  = var.access_key
    secret_key                  = var.secret_key
    key                         = "00-linux-image.tfstate"
    force_path_style            = true
    skip_credentials_validation = true
    skip_requesting_account_id  = true
  }
}