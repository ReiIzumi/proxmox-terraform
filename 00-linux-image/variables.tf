variable "proxmox_endpoint" {
  description = "Proxmox endpoint"
  type = object({
    endpoint = string
    insecure = bool
  })
}

variable "proxmox_auth" {
  description = "Proxmox authentication"
  type = object({
    api_token   = string
    username    = string
    private_key = string
  })
  sensitive = true
}

variable "iso_storage" {
  description = "ISO Storage"
  type = object({
    datastore_id = string
    node_name    = string
  })
}

variable "iso_url" {
  description = "URL to download the cloud-init Linux image"
  type        = string
}
