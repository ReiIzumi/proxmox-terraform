# Proxmox authentication
proxmox_endpoint = {
  endpoint = "https://proxmox-name.intranet:8006/"
  insecure = true
}
proxmox_auth = {
  api_token   = "terraform@pve!provider=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"
  username    = "terraform"
  private_key = "private.ppk"
}

# Proxmox config
proxmox_config = {
  node_name         = "proxmox-name"
  snippet_datastore = "local"
  vm_datastore      = "local-zfs"
  iso               = "pyrrha:iso/ubuntu-24.04-minimal-cloudimg-amd64.img"
}

vm = {
  pool_id       = "test"
  name          = "controlplane-name"
  description   = "Kubernetes Control-Plane - Test environment"
  tags          = ["k8s", "control", "test"]
  startup_order = "1"
  user_name     = "user"
  public_key    = "test.pub"
}

vm_hardware = {
  cpus           = 2
  memory         = 4096
  disk_root_size = 50
  mac_address    = "00:00:00:00:00:00"
}

vm_ntp = {
  ntp_primary   = "ntp1.domain.intranet"
  ntp_secondary = "ntp2.domain.intranet"
}

vm_nagios = {
  nagios_server = "nagios1.domain.intranet,nagios2.domain.intranet"
}

k8s_version = "v1.31"
