terraform {
  required_providers {
    proxmox = {
      source = "bpg/proxmox"
    }
  }
}

provider "proxmox" {
  endpoint = var.proxmox_endpoint.endpoint
  insecure = var.proxmox_endpoint.insecure

  api_token = var.proxmox_auth.api_token
  ssh {
    agent       = true
    username    = var.proxmox_auth.username
    private_key = fileexists(var.proxmox_auth.private_key) ? file(var.proxmox_auth.private_key) : var.proxmox_auth.private_key
  }
}
