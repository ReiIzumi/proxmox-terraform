# Proxmox Terraform
Mini project to generate VMs on Proxmox using Terraform.

It is divided into multiple folders where each one specializes in a type of functionality or VM, not being the best system, but it is moderately functional for creating news.

**All VMs are configured with DHCP, so previously is required to assign a name/IP according to the MAC configured**

## List of functionalities/VM
All templates and VMs was created with the same version of Ubuntu, in other distributions/versions they may not work because the cloud-init configuration.

```
https://cloud-images.ubuntu.com/minimal/releases/noble/release/ubuntu-24.04-minimal-cloudimg-amd64.img
```

- **00-linux-image**: Only download the linux ISO used for the next folders. Since I'm using the same ISO for everything, I don't want to download multiple times the same.
- **01-empty-vm**: Template to deploy a new VM with the Ubuntu Cloud-init with Agent + SSH + Nagios minimum config.
- **01-template-vm**: Same as the empty vm, but to generate a template. Require stop and convert to template manually, it can be done from the UI.
- **02-docker-vm**: Empty docker.
- **03-k8s-ControlPlane** and **03-k8s-Node**: Evolution of the *empty vm* with the requirements to use the VM as a Kubernetes control pane or node. Node has a second disk for Longhorn.
- **04-postgresql**: PostgreSQL configured with the defined databases and the backups.

## How to execute
Since the state contains too much information, they are stored in an S3 and not in the repository, for it, the `backend` contains the information of the bucket. This `backend` is prepared to use something like Minio, not the real AWS S3.

I'm using the [bpg/proxmox](https://registry.terraform.io/providers/bpg/proxmox/latest/docs) as a Proxmox Provider, so the user, credentials, roles and public key must exist to connect with Proxmox.

Configure the S3 credentials in Windows:
```bash
setx TF_VAR_endpoint "https://s3-server.intranet"
setx TF_VAR_bucket "bucket_name"
setx TF_VAR_access_key "access_key"
setx TF_VAR_secret_key "secret_key"
```

or in Linux:
```bash
export TF_VAR_endpoint="https://s3-server.intranet"
export TF_VAR_bucket="bucket_name"
export TF_VAR_access_key="access_key"
export TF_VAR_secret_key="secret_key"
```

Create the `terraform.tfvars` with the common parts:
```vim
proxmox_endpoint = {
  endpoint = "https://proxmox-name.intranet:8006/"
  insecure = true
}

proxmox_auth = {
  api_token   = "terraform@pve!provider=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"
  username    = "terraform"
  private_key = "private.ppk"
}

iso_storage = {
  datastore_id = "local"
  node_name    = "proxmox-name"
}
```

**Remember to fill the specific variables in each case**

Execute the init, plan and apply if everything is correct:

```bash
terraform init
terraform plan
terraform apply -auto-approve
```

or destroy the existing one:
```bash
terraform destroy -auto-approve
```

## Kubernetes
These templates generates one Control Pane and one Node. Multiple servers needs to be deployed (the recommendation is almost 1 Control Pane and 3 Nodes).

The template deploys a new VM prepared with the following:
- Ubuntu according to the ISO previously downloaded
- Assign the user and public key for SSH (password is NOT allowed). That user can do `sudo` without password
- Update Ubuntu and packages
- Install VM dependencies for Proxmox (QEMU Guest Agent)
- Configure NTP to sync with local servers
- Configure and open Nagios Client (with NRPE) for an specific local servers
- Install CRI-O and Kubernetes according to the version indicated (**Both use the same major and minor version. Please check if CRI-O has it available if you change the default one**)
- For *Node*, prepare a second disk according to the `Longhorn` default requirements.

These templates create the Kubernetes but does NOT initialize it, that process must be done manually.

**Network needs to assign an static IP according to the MAC.**

*This is for reusing the templates to add new servers to an existing cluster or to create a new one.*

### Initialize Kubernetes
Connect via SSH to the Control Pane
```bash
sudo systemctl enable kubelet
sudo kubeadm config images pull
sudo kubeadm init --control-plane-endpoint server-name.domain.intranet

sudo mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
kubectl -n kube-system rollout restart deployment coredns
```

*Sometimes it is necessary to restart the `coredns` after adding some nodes or after waiting for everything to deploy.*

### Add node
In the Control Pane:
```bash
sudo kubeadm token create --print-join-command
```

Paste the command adding the `sudo` in each Node:
```bash
sudo kubeadm join server-name.domain.intranet:6443 --token token... --discovery-token-ca-cert-hash sha256:sha...
```

