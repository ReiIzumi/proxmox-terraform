data "local_file" "ssh_public_key" {
  filename = var.vm.public_key
}

resource "proxmox_virtual_environment_file" "cloud_config" {
  node_name    = var.proxmox_config.node_name
  content_type = "snippets"
  datastore_id = var.proxmox_config.snippet_datastore

  source_raw {
    data = <<EOF
#cloud-config
hostname: ${var.vm.name}
users:
  - default
  - name: ${var.vm.user_name}
    groups:
      - sudo
    shell: /bin/bash
    ssh_authorized_keys:
      - ${trimspace(data.local_file.ssh_public_key.content)}
    sudo: ALL=(ALL) NOPASSWD:ALL

package_update: true
package_upgrade: true
packages:
  - qemu-guest-agent
  - net-tools
  - vim
  - nagios-nrpe-server
  - postgresql
  - postgresql-contrib
  - netcat-traditional
  - cron
  - nfs-common
  - libdatetime-perl
  - libtemplate-plugin-datetime-perl

write_files:
  - path: /etc/ssh/banner
    permissions: '0644'
    owner: root:root
    content: |
      ========== ========== ==========
         Welcome to ${var.vm.name}
      ========== ========== ==========
      You are accessing a private server
      Unauthorized access is not allowed

  - path: /etc/systemd/timesyncd.conf
    permissions: '0644'
    owner: root:root
    content: |
      [Time]
      NTP=${var.vm_ntp.ntp_primary} 
      FallbackNTP=${var.vm_ntp.ntp_secondary}

  - path: /etc/nagios/nrpe_local.cfg
    permissions: '0644'
    owner: root:root
    content: |
      allowed_hosts=127.0.0.1,::1,${var.vm_nagios.nagios_server}
      dont_blame_nrpe=1
      command[check_load]=/usr/lib/nagios/plugins/check_load -r -w .80,.75,.70 -c .90,.85,.80
      command[check_disk_root]=/usr/lib/nagios/plugins/check_disk -w 20% -c 10% -p /
      command[check_total_procs]=/usr/lib/nagios/plugins/check_procs -w 400 -c 600
      command[check_bkp]=/opt/scripts/notificationChecker.pl $ARG1$

runcmd:
    - sed -i 's@#Banner none@Banner /etc/ssh/banner@' /etc/ssh/sshd_config
    - systemctl restart ssh
    - systemctl enable qemu-guest-agent
    - systemctl start qemu-guest-agent
    - systemctl restart systemd-timesyncd
    - systemctl enable nagios-nrpe-server
    - systemctl start nagios-nrpe-server
    - echo "Configuring PostgreSQL to allow external connections"
    - PG_CONF_DIR=$(find /etc/postgresql -mindepth 2 -maxdepth 2 -type d -name "main" | head -n 1)
    - sed -i "s/^#listen_addresses = 'localhost'/listen_addresses = '*'/g" "$PG_CONF_DIR/postgresql.conf"
    - echo "host all all 0.0.0.0/0 trust" >> "$PG_CONF_DIR/pg_hba.conf"
    - echo "host all all ::/0 trust" >> "$PG_CONF_DIR/pg_hba.conf"
    - systemctl restart postgresql
    - echo "Downloading backup scripts"
    - mkdir /opt/scripts
    - mkdir /mnt/backup
    - cd /opt/scripts
    - sudo wget https://gitlab.com/ReiIzumi/scripts-backup/-/raw/master/mount.sh
    - sudo wget https://gitlab.com/ReiIzumi/scripts-backup/-/raw/master/rotateFiles.sh
    - sudo wget https://gitlab.com/ReiIzumi/scripts-backup/-/raw/master/notifyCSV.sh
    - sudo wget https://gitlab.com/ReiIzumi/scripts-backup/-/raw/master/notificationChecker.pl
    - sudo wget https://gitlab.com/ReiIzumi/scripts-backup/-/raw/master/backup_PostgreSQL.sh
    - sudo chmod 744 *
    - sudo chmod 745 notificationChecker.pl
    - sudo chmod 744 backup_PostgreSQL.sh
    - echo "Configuring backup in cron"
    - (crontab -l 2>/dev/null; echo "${var.backup_cron} /opt/scripts/backup_PostgreSQL.sh") | crontab -
    - echo "Creating PostgreSQL databases and users"
    - DB_USERS="${var.pg_databases}"
    - |
      for ENTRY in $(echo "$DB_USERS" | tr ';' '\n'); do
        DB_NAME=$(echo "$ENTRY" | cut -d':' -f1)
        PASSWORD=$(echo "$ENTRY" | cut -d':' -f2)
        sudo -u postgres psql -c "CREATE DATABASE \"$DB_NAME\";"
        sudo -u postgres psql -c "CREATE USER \"$DB_NAME\" WITH PASSWORD '$PASSWORD';"
        sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE \"$DB_NAME\" TO \"$DB_NAME\";"
        sudo -u postgres psql -d $DB_NAME -c "GRANT ALL ON SCHEMA public TO \"$DB_NAME\";"
      done
    - echo "Configuring NFS for backups"
    - sed -i "s|^NFS_SERVER=.*|NFS_SERVER=\"${var.backup_nfs.server}\"|" mount.sh
    - sed -i "s|^NFS_FOLDER=.*|NFS_FOLDER=\"${var.backup_nfs.remote_path}\"|" mount.sh
    - echo "Configuring PostgreSQL backup script"
    - DATABASES=$(echo "$DB_USERS" | tr ';' '\n' | cut -d':' -f1 | tr '\n' ' ')
    - sed -i "s|^POSTGRESQL_DATABASES=.*|POSTGRESQL_DATABASES=\"$DATABASES\"|" backup_PostgreSQL.sh
    EOF

    file_name = "cloud-config_${var.vm.name}.yaml"
  }
}

resource "proxmox_virtual_environment_vm" "linux_vm" {
  node_name = var.proxmox_config.node_name

  pool_id     = var.vm.pool_id
  name        = var.vm.name
  description = var.vm.description
  tags        = var.vm.tags

  operating_system {
    type = "l26" # Linux Kernel 2.6 - 5.X.
  }
  keyboard_layout = "es"

  startup {
    order      = var.vm.startup_order
    up_delay   = "60"
    down_delay = "60"
  }

  agent {
    enabled = true
  }

  cpu {
    cores = var.vm_hardware.cpus
    type  = "x86-64-v2-AES" # >= Intel Westmere (2006)
  }

  memory {
    dedicated = var.vm_hardware.memory
  }

  disk {
    datastore_id = var.proxmox_config.vm_datastore
    file_id      = var.proxmox_config.iso
    interface    = "virtio0"
    discard      = "on"
    size         = var.vm_hardware.disk_size
  }

  network_device {
    mac_address = var.vm_hardware.mac_address
  }

  initialization {
    datastore_id = var.proxmox_config.vm_datastore
    ip_config {
      ipv4 {
        address = "dhcp"
      }
    }

    user_data_file_id = proxmox_virtual_environment_file.cloud_config.id
  }
}
