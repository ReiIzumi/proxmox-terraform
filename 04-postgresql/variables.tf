variable "proxmox_endpoint" {
  description = "Proxmox endpoint"
  type = object({
    endpoint = string
    insecure = bool
  })
}
variable "proxmox_auth" {
  description = "Proxmox authentication"
  type = object({
    api_token   = string
    username    = string
    private_key = string
  })
  sensitive = true
}

variable "proxmox_config" {
  description = "Proxmox details to deploy VMs"
  type = object({
    node_name         = string
    snippet_datastore = string
    vm_datastore      = string
    iso               = string
  })
}

variable "vm" {
  description = "VM details"
  type = object({
    pool_id       = string
    name          = string
    description   = string
    tags          = list(string)
    startup_order = string
    user_name     = string
    public_key    = string
  })
}

variable "vm_hardware" {
  description = "VM hardware details"
  type = object({
    cpus        = number
    memory      = number
    disk_size   = number
    mac_address = string
  })
}

variable "vm_ntp" {
  description = "NTP details"
  type = object({
    ntp_primary   = string
    ntp_secondary = string
  })
}

variable "vm_nagios" {
  description = "Name of the Nagios server to allow the access. Can be multiple separated by comma"
  type = object({
    nagios_server = string
  })
}

variable "pg_databases" {
  description = "PostgreSQL databases and passwords in dbname1:password1;dbname2:password2 format"
  type        = string
}

variable "backup_nfs" {
  description = "Server info to store the backups"
  type = object({
    server      = string
    remote_path = string
  })
}

variable "backup_cron" {
  description = "Time when the backup will start in cron format (00 10 * * *)"
  type        = string
}
