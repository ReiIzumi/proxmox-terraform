data "local_file" "ssh_public_key" {
  filename = var.vm.public_key
}

resource "proxmox_virtual_environment_file" "cloud_config" {
  node_name    = var.proxmox_config.node_name
  content_type = "snippets"
  datastore_id = var.proxmox_config.snippet_datastore

  source_raw {
    data = <<EOF
#cloud-config
hostname: ${var.vm.name}
users:
  - default
  - name: ${var.vm.user_name}
    groups:
      - sudo
    shell: /bin/bash
    ssh_authorized_keys:
      - ${trimspace(data.local_file.ssh_public_key.content)}
    sudo: ALL=(ALL) NOPASSWD:ALL

package_update: true
package_upgrade: true
packages:
  - qemu-guest-agent
  - net-tools
  - vim
  - nagios-nrpe-server

write_files:
  - path: /etc/ssh/banner
    permissions: '0644'
    owner: root:root
    content: |
      ========== ========== ==========
         Welcome to ${var.vm.name}
      ========== ========== ==========
      You are accessing a private server
      Unauthorized access is not allowed

  - path: /etc/systemd/timesyncd.conf
    permissions: '0644'
    owner: root:root
    content: |
      [Time]
      NTP=${var.vm_ntp.ntp_primary} 
      FallbackNTP=${var.vm_ntp.ntp_secondary}

  - path: /etc/nagios/nrpe_local.cfg
    permissions: '0644'
    owner: root:root
    content: |
      allowed_hosts=127.0.0.1,::1,${var.vm_nagios.nagios_server}      
      command[check_load]=/usr/lib/nagios/plugins/check_load -r -w .80,.75,.70 -c .90,.85,.80
      command[check_disk_root]=/usr/lib/nagios/plugins/check_disk -w 20% -c 10% -p /
      command[check_total_procs]=/usr/lib/nagios/plugins/check_procs -w 400 -c 600

runcmd:
    - sed -i 's@#Banner none@Banner /etc/ssh/banner@' /etc/ssh/sshd_config
    - systemctl restart ssh
    - systemctl enable qemu-guest-agent
    - systemctl start qemu-guest-agent
    - systemctl restart systemd-timesyncd
    - systemctl enable nagios-nrpe-server
    - systemctl start nagios-nrpe-server
    EOF

    file_name = "cloud-config_${var.vm.name}.yaml"
  }
}

resource "proxmox_virtual_environment_vm" "linux_vm" {
  node_name   = var.proxmox_config.node_name
  vm_id       = var.vm.vm_id
  pool_id     = var.vm.pool_id
  name        = var.vm.name
  description = var.vm.description
  tags        = var.vm.tags

  operating_system {
    type = "l26" # Linux Kernel 2.6 - 5.X.
  }
  keyboard_layout = "es"

  startup {
    order      = var.vm.startup_order
    up_delay   = "60"
    down_delay = "60"
  }

  agent {
    enabled = true
  }

  cpu {
    cores = var.vm_hardware.cpus
    type  = "x86-64-v2-AES" # >= Intel Westmere (2006)
  }

  memory {
    dedicated = var.vm_hardware.memory
  }

  disk {
    datastore_id = var.proxmox_config.vm_datastore
    file_id      = var.proxmox_config.iso
    interface    = "virtio0"
    discard      = "on"
    size         = var.vm_hardware.disk_size
  }

  network_device {
    mac_address = var.vm_hardware.mac_address
  }

  initialization {
    datastore_id = var.proxmox_config.vm_datastore
    ip_config {
      ipv4 {
        address = "dhcp"
      }
    }

    user_data_file_id = proxmox_virtual_environment_file.cloud_config.id
  }
}
